---
release_number: "17.9" # version number - required
title: "GitLab 17.9 released with GitLab Duo Self-Hosted available in GA" # short title (no longer than 62 characters) - required
author: Rutvik Shah # author name and surname - required
author_gitlab: rutshah # author's gitlab.com username - required
image_title: '/images/17_9/product-gl17-blog-release-cover-17-9-0093-1800x945-fy25.png' # cover image - required
description: "GitLab 17.9 released with GitLab Duo Self-Hosted available in GA,the ability to run multiple GitLab Pages sites with parallel deployments, the option to add project files to Duo Chat in VS Code and JetBrains IDEs, automatic deletion of older pipelines and much more!" # short description - required
twitter_image: '/images/17_9/product-gl17-blog-release-cover-17-9-0093-1800x945-fy25.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image



---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.9 with [GitLab Duo Self-Hosted available in GA](#gitlab-duo-self-hosted-is-generally-available), [the ability to run multiple GitLab Pages sites with parallel deployments](#run-multiple-pages-sites-with-parallel-deployments), [the option to add project files to Duo Chat in VS Code and JetBrains IDEs](#add-project-files-to-duo-chat-in-vs-code-and-jetbrains-ides), [automatic deletion of older pipelines](#automatic-cicd-pipeline-cleanup) and much more!

These are just a few highlights from the 110+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 322 contributions you provided to GitLab 17.9!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!
