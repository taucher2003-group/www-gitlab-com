---
title: "GitLab Patch Release: 17.0.7"
categories: releases
author: Ahmad Tolba
author_gitlab: ahyield
author_twitter: gitlab
description: "GitLab releases 17.0.7"
tags: patch releases, releases
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.0.7 for GitLab Community Edition and Enterprise Edition.

These versions resolve a number of regressions and bugs. This patch release does not include any security fixes.

## GitLab Community Edition and Enterprise Edition

### 17.0.7

* [Backport 17.0: Release Environments - pipeline level resource group](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161490)
* [Backport 17.0: Build assets image when running release environments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161902)
* [Backport 17.0 - Do not run release-environments on tagging](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162294)
* [Backport canonical RE downstream pipeline removal](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162494)
* [Update minimum Go version requirement for self-compiled (17.0)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162772)
* [Backport 17.0: Always build assets image when tagging](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/163498)
* [Backport gitlab-qa shm fix 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161553)
* [CI: Add test basic package functionality before release (17.0 Backport)](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7880)
* [Use latest builder images for check-packages pipeline (17.0 Backport)](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7878)
* [[17.0 Backport] Deprecate CentOS 7](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7902)
* [Backport to 17.0: Fix JobArtifactState query timeout](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/165271)

## Important notes on upgrading

This version does not include any new migrations, and for multi-node deployments, [should not require any downtime](https://docs.gitlab.com/ee/update/#upgrading-without-downtime).

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-reconfigure`](https://docs.gitlab.com/ee/update/zero_downtime.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](/update/).

## GitLab subscriptions

Access to GitLab Premium and Ultimate features is granted by a paid [subscription](/pricing/).

Alternatively, [sign up for GitLab.com](https://gitlab.com/users/sign_in)
to use GitLab's own infrastructure.

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
