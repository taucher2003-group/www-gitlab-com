---
title: "GitLab Patch Release: 17.6.2, 17.5.4, 17.4.6"
categories: releases
author: Costel Maxim
author_gitlab: cmaxim
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.6.2, 17.5.4, 17.4.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/12/11/patch-release-gitlab-17-6-2-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.6.2, 17.5.4, 17.4.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Injection of Network Error Logging (NEL) headers in kubernetes proxy response could lead to account takeover abusing OAuth flows](#injection-of-network-error-logging-nel-headers-in-kubernetes-proxy-response-could-lead-to-ato-abusing-oauth-flows) | High |
| [Denial of Service by repeatedly sending unauthenticated requests for diff-files](#denial-of-service-by-repeatedly-sending-unauthenticated-requests-for-diff-files) | High |
| [CI_JOB_TOKEN could be used to obtain GitLab session](#ci_job_token-could-be-used-to-obtain-gitlab-session) | Medium |
| [Open redirect in releases API](#open-redirect-in-releases-api) | Medium |
| [Client-Side Path Traversal in Harbor artifact links](#client-side-path-traversal-in-harbor-artifact-links) | Medium |
| [HTML injection in vulnerability details could lead to Cross Site Scripting](#html-injection-in-vulnerability-details-could-lead-to-cross-site-scripting) | Medium |
| [Leak branch names of projects with confidential repository](#leak-branch-names-of-projects-with-confidential-repository) | Medium |
| [Non member can view unresolved threads marked as internal notes](#non-member-can-view-unresolved-threads-marked-as-internal-notes) | Medium |
| [Uncontrolled Resource Consumption through a maliciously crafted  file](#uncontrolled-resource-consumption-through-a-maliciously-crafted--file) | Medium |
| [Certain sensitive information passed as literals inside GraphQL mutations retained in GraphQL logs](#certain-sensitive-information-passed-as-literals-inside-graphql-mutations-retained-in-graphql-logs) | Medium |
| [Information disclosure of confidential incidents details to a group member in Gitlab Wiki](#information-disclosure-of-confidential-incidents-details-to-a-group-member-in-gitlab-wiki) | Low |
| [Domain Confusion in GitLab Pages Unique Domain Implementation](#domain-confusion-in-gitlab-pages-unique-domain-implementation) | Low |

### Injection of Network Error Logging (NEL) headers in kubernetes proxy response could lead to ATO abusing OAuth flows

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.1 before 17.4.6, starting from 17.5 before 17.5.4, and starting from 17.6 before 17.6.2, injection of Network Error Logging (NEL) headers in kubernetes proxy response could lead to session data exfiltration.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N), 8.7).
It is now mitigated in the latest release and is assigned [CVE-2024-11274](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-11274).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of Service by repeatedly sending unauthenticated requests for diff-files

An issue has been discovered in GitLab CE/EE affecting all versions from 9.4 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2. An attacker could cause a denial of service with requests for diff files on a commit or merge request.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H), 7.5).
It is now mitigated in the latest release and is assigned [CVE-2024-8233](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8233).

Thanks [a92847865](https://hackerone.com/a92847865) for reporting this vulnerability through our HackerOne bug bounty program.


### CI_JOB_TOKEN could be used to obtain GitLab session

An issue has been discovered in GitLab CE/EE affecting all versions starting from 13.7 before 17.4.6, from 17.5 before 17.5.4, and from 17.6 before 17.6.2. It may have been possible for an attacker with a victim's `CI_JOB_TOKEN` to obtain a GitLab session token belonging to the victim.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:L), 6.7).
It is now mitigated in the latest release and is assigned [CVE-2024-12570](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-12570).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Open redirect in releases API

An issue was discovered in GitLab CE/EE affecting all versions from 11.8 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2. An attacker could potentially perform an open redirect against a given releases API endpoint.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N), 6.4).
It is now mitigated in the latest release and is assigned [CVE-2024-9387](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9387).

Thanks [swiftee](https://hackerone.com/swiftee) for reporting this vulnerability through our HackerOne bug bounty program.


### Client-Side Path Traversal in Harbor artifact links

An issue was discovered in GitLab affecting all versions starting 15.2 before 17.4.6, 17.5 before  17.5.4, and 17.6 before 17.6.2. On self hosted installs, it was possible to leak the cross site request forgery (CSRF) token to an external site while the Harbor integration was enabled.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N), 5.4).
It is now mitigated in the latest release and is assigned [CVE-2024-8647](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8647).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### HTML injection in vulnerability details could lead to Cross Site Scripting

An issue has been discovered in GitLab CE/EE affecting all versions from 17.3 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2. Improper output encoding could lead to Cross Site Scripting (XSS) if Content Security Policy (CSP) is not enabled.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N), 5.4).
It is now mitigated in the latest release and is assigned [CVE-2024-8179](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8179).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Leak branch names of projects with confidential repository

An issue has been discovered in GitLab CE/EE affecting all versions from 16.9 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2. By using a specific GraphQL query, under specific conditions an unauthorised user can retrieve branch names.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N), 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-8116](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8116).

Thanks [shells3c](https://hackerone.com/shells3c) for reporting this vulnerability through our HackerOne bug bounty program.


### Non member can view unresolved threads marked as internal notes

An issue was discovered in GitLab CE/EE affecting all versions from 15.0 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2 that allowed non-member users to view unresolved threads marked as internal notes in public projects merge requests.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N), 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-8650](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8650).

Thanks [salh4ckr](https://hackerone.com/salh4ckr) for reporting this vulnerability through our HackerOne bug bounty program.


### Uncontrolled Resource Consumption through a maliciously crafted  file

An issue was discovered in GitLab CE/EE affecting all versions starting from 13.9 before 17.4.6, 17.5 before 17.5.4, and 17.6 before 17.6.2, that allows an attacker to cause uncontrolled resource consumption, potentially leading to a Denial of Service (DoS) condition while parsing templates to generate changelogs.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-9367](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9367).

Thanks [l33thaxor](https://hackerone.com/l33thaxor) for reporting this vulnerability through our HackerOne bug bounty program.


### Certain sensitive information passed as literals inside GraphQL mutations retained in GraphQL logs

An issue was discovered in GitLab CE/EE affecting all versions starting from 11.0 before 17.4.6, starting from 17.5 before 17.5.4, and starting from 17.6 before 17.6.2, where sensitive information passed in GraphQL mutations may have been retained in GraphQL logs.
This is a medium severity issue ([`CVSS:3.1/AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N), 4.0).
It is now mitigated in the latest release and is assigned [CVE-2024-12292](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-12292).

This issue was discovered internally by GitLab team member [Radamanthus Batnag](https://gitlab.com/radbatnag).


### Information disclosure of confidential incidents details to a group member in Gitlab Wiki

An issue has been discovered in GitLab EE affecting all versions starting from 14.3 before 17.4.6, all versions starting from 17.5 before 17.5.4 all versions starting from 17.6 before 17.6.2, that allows group users to view confidential incident title through the Wiki History Diff feature, potentially leading to information disclosure.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N), 3.1).
It is now mitigated in the latest release and is assigned [CVE-2024-10043](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-10043).

Thanks [mateuszek](https://hackerone.com/mateuszek) for reporting this vulnerability through our HackerOne bug bounty program.


### Domain Confusion in GitLab Pages Unique Domain Implementation

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.3 before 17.4.2, all versions starting from 17.5 before 17.5.4, all versions starting from 17.6 before 17.6.2. This issue allows an attacker to create a group with a name matching an existing unique Pages domain, potentially leading to domain confusion attacks.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:L), 3.1).
It is now mitigated in the latest release and is assigned [CVE-2024-9633](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9633).

Thanks [psycho_012](https://hackerone.com/psycho_012) for reporting this vulnerability through our HackerOne bug bounty program.


## Bug fixes


### 17.6.2

* [Upgrade to Postgres 16.6 for client libraries, openssl 3.2](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2120)
* [Fix 401 errors when installing the GitLab for Jira app](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173193)
* [Backport 'dattang/allow-release-environments-to-fail' to 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174008)
* [Backport 'always-build-qa-image-for-release-environments' to 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174254)
* [Add guard clause to Wiki#find_page when title is nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174291)
* [Merge branch '498768-graphql-subscriptions-ignore-unauthorized-error' into '17-6-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174583)
* [Merge branch 'nd/fix-progressbar-progress' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174512)
* [Backport 17-6 Remove unused matched_lines_count](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174700)
* [Backport Zoekt indices without zoekt_repositories stuck in initializing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174701)
* [Backport 'Zoekt: Do not process failed repos' into 17.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174549)
* [Bump devfile gem to 0.1.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174214)

### 17.5.4

* [Upgrade to Postgres 16.6 for client libraries, openssl 3.2](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2121)
* [Fix 401 errors when installing the GitLab for Jira app](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173196)
* [Backport 'always-build-qa-image-for-release-environments' to 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174255)
* [Merge branch '498768-graphql-subscriptions-ignore-unauthorized-error' into '17-5-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174581)
* [Backport https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170141 into 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171140)
* [Quarantine Custom model features specs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175190)

### 17.4.6

* [Upgrade to Postgres 16.6 for client libraries, openssl 3.2](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2122)
* [Add param filtering to avoid error while saving project settings](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173428)
* [Fix 401 errors when installing the GitLab for Jira app](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173197)
* [Backport 'always-build-qa-image-for-release-environments' to 17.4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174256)
* [Backport fix for flaky tests in search_results spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174756)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
