---
release_number: "17.4" # version number - required
title: "GitLab 17.4 released with improved context in GitLab Duo" # short title (no longer than 62 characters) - required
author: Alex Martin # author name and surname - required
author_gitlab: alex_martin # author's gitlab.com username - required
image_title: '/images/17_4/17_4-cover-image.png' # cover image - required
description: "GitLab 17.4 released with more context-aware GitLab Duo Code Suggestions using open tabs, auto-merge when all checks pass, extension marketplace in the Web IDE, list Kubernetes resource events and much more!" # short description - required
twitter_image: '/images/17_4/17_4-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

Today, we are excited to announce the release of GitLab 17.4 with [more context-aware Code Suggestions using open tabs](#more-context-aware-gitlab-duo-code-suggestions-using-open-tabs), [auto-merging when all checks pass](#auto-merge-when-all-checks-pass), [extension marketplace in the Web IDE](#extension-marketplace-now-available-in-the-web-ide), [Advanced SAST generally available](#advanced-sast-is-generally-available) and much more!

These are just a few highlights from the 140+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 220+ contributions you provided to GitLab 17.4!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month's release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.5 release kickoff video.
