---
layout: markdown_page
title: 2024 GitLab Wheel of Fun Promotion - Official Rules
description: 2024 GitLab Wheel of Fun Promotion - Official Rules
---

NO PURCHASE NECESSARY TO ENTER OR WIN. MAKING A PURCHASE OR PAYMENT OF ANY
KIND WILL NOT INCREASE YOUR CHANCES OF WINNING. VOID WHERE PROHIBITED OR
RESTRICTED BY LAW.

**1. PROMOTION DESCRIPTION:** The 2024 GitLab Wheel of Fun Promotion ("Promotional Game")
begins on 26 September 2024 at 8:00 am PT (11:00 am ET; 3:00 pm UTC) and ends on 26
September 2024 at 5:20 pm PT (8:20 pm ET; 12:20 am UTC), and the sign-up period for the
Promotional Game begins on 23 September 2024 at 9:00 am PT (12:00 pm ET; 4:00 pm UTC)
and ends when all of the spots on the sign-up Agenda are filled or by 26 September 2024 at
4:30 pm PT (7:30 pm ET; 11:30 pm UTC) (the "Promotion Period"). The Promotional Game will be
included in an announcement for Compliance Day, on 23 September 2024 at 9:00 am PT (12:00
pm ET; 4:00 pm UTC) on the Slack channel #whats-happening-at-GitLab.

The sponsor of this Promotional Game is GitLab, Inc. ("Sponsor"). By participating in the
Promotional Game, each Entrant unconditionally accepts and agrees to comply with and abide by
these Official Rules and the decisions of Sponsor, which shall be final and binding in all respects.
Sponsor is responsible for the collection, submission or processing of Entries and the overall
administration of the Promotional Game. Entrants should look solely to Sponsor with any
questions, comments or problems related to the Promotional Game. Sponsor may be reached by
email at rpack@gitlab.com during the Promotion Period.

**2. ELIGIBILITY:** Open only to legal residents worldwide who are GitLab team members and have
a GitLab.com email address, and who are at the age of majority in their locale (the "Entrant").
Household Members and Immediate Family Members of such individuals are also not eligible to enter
or win. "Household Members" shall mean those people who share the same residence at least three 
months a year. "Immediate Family Members" shall mean parents, step-parents, legal guardians, children, step-children, siblings, step-siblings, or spouses. This Promotional Game is subject to all applicable
federal, state and local laws and regulations and is void where prohibited or restricted by law.

**3. PRIZES:** Up to ten (10) winners will each receive one (1) meal, expensed in an amount not to
exceed USD $30.00 (including tax and tip), or the equivalent in local currency, based on the
exchange rate of the day the meal is purchased, using the [OANDA Currency Converter](https://www.oanda.com/currency-converter/en/?from=EUR&to=USD&amount=25). **Winners
are responsible for ensuring that the amount does not exceed USD$30.00, prior to submitting
their expense.** Any expense beyond USD $30.00, or the equivalent in local currency, will be the
responsibility of the winner. Approximate retail value (“ARV”): up to USD $30.00, or the
equivalent in local currency.

**SEE SECTION 7, WINNER NOTIFICATION AND PRIZE REDEMPTION, FOR INSTRUCTIONS ON
HOW TO REDEEM THE PRIZE.**

**Total value of all prizes: Up to USD $300.00, or the equivalent in local currency.**

Only one prize per person and per household will be awarded. Gift cards and gift certificates are
subject to the terms and conditions of the issuer. Prizes cannot be transferred, redeemed for cash
or substituted by winner. Sponsor reserves the right in its sole and absolute discretion to award a
substitute prize of equal or greater value if a prize described in these Official Rules is unavailable
or cannot be awarded, in whole or in part, for any reason. The ARV of the prize represents
Sponsor's good faith determination. That determination is final and binding and cannot be
appealed. If the actual value of the prize turns out to be less than the stated ARV, the difference
will not be awarded in cash. Sponsor makes no representation or warranty concerning the
appearance, safety or performance of any prize awarded. Restrictions, conditions, and limitations
may apply. Sponsor will not replace any lost or stolen prize items.

Prizes will only be awarded and/or delivered to addresses within said locations. All federal, state,
provincial and/or local taxes, fees, and surcharges are the sole responsibility of the prize winner.
Failure to comply with the Official Rules will result in forfeiture of the prize.

**4. HOW TO ENTER:** Enter the Promotional Game during the Promotion Period online by:
Entering your name on a sign-up sheet (“Agenda”), available on 23 September 2024, in a Slack announcement for Compliance Day on the Slack channel, #whats-happening-at-GitLab. **SIGN UP IS ON A FIRST COME, FIRST SERVED BASIS.**
You will be required to choose one (1) of five (5) rounds (“Rounds”) in one (1) of the two (2) fifty (50) minute sessions (“Session 1” or “Session 2”) in which to participate as a player (“Player”), to be held on 26 September 2024 at the following times:
Session 1: Starts at 8:00 am PT (11:00 am ET; 3:00 pm UTC) and ends at approximately 8:50 am PT (11:50 am ET; 3:50 pm UTC)
○ Session 2: Starts at 4:30 pm PT (7:30 pm ET; 11:30 pm UTC) and ends at approximately 5:20 pm PT (8:20 pm ET; 12:20 am UTC).
Each Round will consist of three (3) Players.
If all of the spots in each of the Rounds are filled, you can enter your name as a potential participant on standby on the Agenda. If a Player is not present for their Round, the names on the standby list will be called in the order in which they appear, to participate in that Round. If you are not present when your name is called, your spot will be forfeited and the next name on the list will be called.

**Players may only participate in one Session and in one Round.**

**5. HOW TO PLAY:**

During your Session, you will be eligible to play one (1) Round.
There will be three (3) Players in each Round.
The order of Players will be determined by the order in which Players add their name to the Agenda, on a first come, first served basis.
The Round will begin with the introduction of a word puzzle, the solution to which will be a privacy-related phrase. Puzzle topics are at the sole discretion of Sponsor.
Players in each Round will be shown a board with blank squares. Behind the squares are the letters which make up the word puzzle.
Players will each take a turn. Players will have the option to either spin the wheel (“Wheel”), buy a vowel, or solve the puzzle.
For the Game, the letter “Y” will be treated as a consonant.

*The Wheel:*

The Wheel is divided into sections, representing either a game dollar value or sections with, “Lose a Turn” or “Bankrupt.” **The game dollar value represents points, not actual dollars and is for the sole purpose of allowing Players to accumulate points represented as dollars, to purchase vowels (“Points”). Prize money equivalent to the accumulated game dollar value will not be awarded.**
The Round will start with the first Player spinning the Wheel, using the “Spin/Stop” button.
The Player will click the button to start the Wheel spinning, then click the button again to stop the Wheel. If the Wheel stops at a section with a game dollar value, the Player will select a letter (consonants only) for the Player’s first round.
If the letter is in the puzzle, the game dollar value on the Wheel will go into the Player’s Points. If the letter is in the puzzle more than once, the game dollar value will be multiplied by the number of times that letter appears in the puzzle. The Player will continue until:
the letter requested is already in the puzzle; or,
the letter requested is not in the puzzle; or,
they land on “Lose a Turn” or “Bankrupt”; or,
they attempt to attempt to purchase a vowel which is not in the puzzle; or,
they incorrectly guess the puzzle
Vowels can be purchased for two hundred and fifty ($250.00) game dollars. If a vowel appears in a puzzle more than once, the Player will need to purchase each vowel for $250.00 game dollars Points. Vowels can only be purchased after the Player has enough game dollars Points to cover the purchase of the vowels.
If a Player spins the Wheel and it stops at “Lose a Turn”, the Player’s turn ends and the next Player takes a turn.
If a Player spins the Wheel and it stops at “Bankrupt”, the Player will lose all of the Points in their score and will lose their turn.
If the letter that the Player chooses is in the puzzle, then the Player’s turn will continue.
Players can solve the puzzle at the beginning of their turn or after they choose a letter that appears in the puzzle.
If at a Player’s turn, the only letters remaining in the puzzle are vowels, the Wheel will not be spun and that Player can either solve the puzzle, or purchase a vowel and then attempt to solve the puzzle. If the vowel that the Player chooses is not in the puzzle, the Player will not be given the opportunity to solve the puzzle and the next Player takes his/her turn.
If a Player attempts to solve the puzzle and makes a mistake, they will have the opportunity to correct the mistake.
The Round ends when a Player correctly solves the puzzle.

Automated or robotic Entries submitted by individuals or organizations will be disqualified.
Internet entry must be made by the Entrant. Any attempt by Entrant to obtain more than the
stated number of Entries by using multiple/different email addresses, identities, registrations,
logins or any other methods, including, but not limited to, commercial contest/Promotional Game
subscription notification and/or entering services, will void Entrant's Entries and that Entrant may
be disqualified. Final eligibility for the award of any prize is subject to eligibility verification as set
forth below. All Entries must be posted by the end of the Promotion Period in order to participate.
Sponsor's database clock will be the official timekeeper for this Promotional Game.

**6. WINNER SELECTION:** The Winner(s) of the Promotional Games are Players who accurately
solve a puzzle in the Round in which they are participating, from among all eligible Entries in that
Round. Winners are confirmed by Sponsor or its designated representatives, whose decisions are
final. Odds of winning will vary depending on the number of eligible Entries received.

**7. WINNER NOTIFICATION AND PRIZE REDEMPTION:** Winners will be notified live when a
Player accurately solves a puzzle during the Round in which the Player is participating. Winners
will subsequently be sent an email to their GitLab.com email address to confirm that they’re a
winner, with instruction on redemption of the prize. Instructions will include the prize description
and indicate the relevant Navan expense category and classification that they must use when
submitting the expense. Team members in all non-U.S. countries must provide a receipt. The
meal must be expensed in Navan and must be redeemed no later than thirty (30) days from the
date of the notification email. The expense must be submitted within sixty (60) days after the date
of the meal purchase, in accordance with Sponsor’s Travel and Expense Policy. Potential Winner
must accept a prize by email as directed by Sponsor within 10 days of notification. Sponsor is not
responsible for any delay or failure to receive notification for any reason, including inactive email
account(s), technical difficulties associated therewith, or Winner’s failure to adequately monitor
any email account.

Any winner notification not responded to or returned as undeliverable may result in prize
forfeiture. The potential prize winner may be required to sign and return an affidavit of eligibility
and release of liability, and a Publicity Release (collectively "the Prize Claim Documents"). No
substitution or transfer of a prize is permitted except by Sponsor.

**8. PRIVACY:** Any personal information supplied by you will be subject to the privacy policy of the
Sponsor posted at about.gitlab.com/privacy. By entering the Promotional Game, you grant
Sponsor permission to share your email address and any other personally identifiable information
with the other Promotional Game Entities for the purpose of administration and prize fulfillment,
including use in a publicly available Winners list.

**9. LIMITATION OF LIABILITY:** Sponsor assumes no responsibility or liability for (a) any incorrect
or inaccurate entry information, or for any faulty or failed electronic data transmissions; (b) any
unauthorized access to, or theft, destruction or alteration of entries at any point in the operation
of this Promotional Game; (c) any technical malfunction, failure, error, omission, interruption,
deletion, defect, delay in operation or communications line failure, regardless of cause, with
regard to any equipment, systems, networks, lines, satellites, servers, camera, computers or
providers utilized in any aspect of the operation of the Promotional Game; (d) inaccessibility or
unavailability of any network or wireless service, the Internet or website or any combination
thereof; (e) suspended or discontinued Internet, wireless or landline phone service; or (f) any
injury or damage to participant's or to any other person’s computer or mobile device which may
be related to or resulting from any attempt to participate in the Promotional Game or download of
any materials in the Promotional Game.

If, for any reason, the Promotional Game is not capable of running as planned for reasons which
may include without limitation, infection by computer virus, tampering, unauthorized intervention,
fraud, technical failures, or any other causes which may corrupt or affect the administration,
security, fairness, integrity or proper conduct of this Promotional Game, the Sponsor reserves the
right at its sole discretion to cancel, terminate, modify or suspend the Promotional Game in whole
or in part. In such event, Sponsor shall immediately suspend all drawings and prize awards, and
Sponsor reserves the right to award any remaining prizes (up to the total ARV as set forth in
these Official Rules) in a manner deemed fair and equitable by Sponsor. Sponsor and Released
Parties shall not have any further liability to any participant in connection with the Promotional
Game.

**10. WINNER LIST/OFFICIAL RULES:** To view these Official Rules visit
https://about.gitlab.com/community/sweepstakes/. The winner list will be posted in the Slack
channel, #whats-happening-at-GitLab, on 27 September 2024, after confirmation of winners is
Complete.

**11. SPONSOR:** GitLab, Inc., 268 Bush Street, #350, San Francisco, CA 94104, rpack@gitlab.com
