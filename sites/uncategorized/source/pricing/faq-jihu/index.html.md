---
layout: markdown_page
title: JiHu FAQ
description: "Review frequently asked questions for JiHu and its relationship to GitLab Inc."
canonical_path: "/pricing/faq-jihu/"
---

# JiHu FAQ  
{:.no_toc}

### On this page
{:.no_toc}

{:toc}
- TOC

Thank you for your interest in JiHu. For an overview of JiHu and its relationship to GitLab Inc., please see the [GitLab blog](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/). 

Our FAQ is designed to answer questions that you might have.

## JiHu Overview

Q. **Why did GitLab Inc. license its technology to an independent Chinese company?**

Licensing GitLab’s technology to JiHu will help drive adoption of the GitLab complete DevOps platform in China and foster the GitLab community and open source contributions. 

Q. **Is JiHu fully operational?**

Yes. JiHu is distributing the GitLab JH “JiHu Edition” and fully supporting and serving its customers in China, Hong Kong, and Macau.

Q. **What is GitLab Inc.’s involvement in JiHu?**

   * GitLab Inc. has licensed its technology and brand to JiHu solely to serve customers in China, Macau, and Hong Kong. JiHu provides a specific Chinese distribution of GitLab’s DevOps Platform - available as both a self-managed and SaaS offering (GitLab.cn) - that is constantly updated, has readily accessible features, and tailored software for Chinese companies. JiHu’s SaaS service (GitLab.cn) and GitLab Inc.’s SaaS service (GitLab.com) will share no common infrastructure, networking connectivity, systems, services, data, or resources.
   * To ensure the new company is set up for success, GitLab Inc. has provided initial engineering, marketing, and business support to JiHu as the company gets ready to launch.
   * While GitLab Inc. is the inspiration for JiHu, it is an independent company and can adjust for the local market as it sees fit. 

Q. **From a technical standpoint, how will the JiHu Edition (JH) differ from the Community Edition (CE) and Enterprise Edition (EE)?**

The [JiHu Edition](https://docs.gitlab.com/omnibus/jihu_edition.html) is localized for its market, which means some features in JH may not be present in CE or EE. We will not be trying to maintain feature parity between the JH and EE distributions. GitLab Inc. has a cross-license agreement with JiHu which allows GitLab Inc. the rights to use and distribute JiHu’s proprietary features or code. For example, in the future, features that JiHu creates that GitLab Inc. finds useful may be selected and included in the EE distribution. 

Some examples of features JiHu may build include:

   * Integrations with local cloud providers (Alicloud, Tencent cloud, Baidu cloud)
   * Integrations with local chat apps (WeChat, DingTalk)
   * Features which are prominent in Chinese markets but not yet in the West.

## Customer Details

Q. **Why would customers want to transition to JiHu?**

JiHu was introduced to provide a localized GitLab solution and support for users based in China, Macau, and Hong Kong. Beyond offering the tailored JiHu Edition, JiHu will also provide local support services in Mandarin. Some customers may prefer to work with a local Chinese company. You can learn more about JiHu on its website: [http://about.gitlab.cn/](http://about.gitlab.cn/). You can also contact JiHu directly at [customer-transition@gitlab.cn](mailto:customer-transition@gitlab.cn).

Q. **Is JiHu ready to support customers?**

JiHu is currently serving self-managed customers and SaaS customers.

Q. **How can I get more details on JiHu’s services and plans?**

You can learn more about JiHu on its website: [http://about.gitlab.cn/](http://about.gitlab.cn/). You can also contact JiHu directly at [customer-transition@gitlab.cn](mailto:customer-transition@gitlab.cn).

Q. **What will happen to my self-managed data on GitLab CE or EE if I transition to JiHu?**

Your data on your self-managed GitLab CE or EE instance will remain on your self-managed instance. If you choose to [upgrade to the latest version of GitLab JH](https://docs.gitlab.com/omnibus/jihu_edition.html), your data will remain on the self-managed instance. Please note that this does excludes [Usage Ping](https://docs.gitlab.com/ee/development/usage_ping/index.html#disable-usage-ping) which provides high-level data to help JiHu's product, support, and sales teams.

## Partner Details

Q. **Will existing GitLab Inc. partners be able to sell the GitLab DevOps Platform in China, Macau, and Hong Kong?**

Partners will only be able to sell into China, Macau, and Hong Kong if they sign a partnership agreement with JiHu. 

Q. **When is the last date for GitLab Inc. partners to register deals in China, Hong Kong or Macau?**

2021-03-24

Q. **Does my company have to enter into a partnership arrangement with JiHu?**

   * To continue selling a GitLab DevOps solution in China, Macau, and Hong Kong, partners will have to enter into a new partnership agreement with JiHu.
   * Partners that sell into APAC and other regions do not need to sign with JiHu. However, existing GitLab Inc. partners in the APAC region will need to sign a contract amendment that eliminates the ability to sell the GitLab DevOps solution in China, Hong Kong and Macau on behalf of GitLab Inc.

Q. **Is JiHu ready to support new partners?**

Yes. JiHu has successfully onboarded a number of partners.

Q. **How can I get more details on JiHu’s plans and program for partners?**

Please contact [partners@gitlab.cn](mailto:partners@gitlab.cn) for more information.

## Additional Questions

Q. **Should I expect to hear from the existing account team?**

You should not expect to hear from your account team. The GitLab transition team and JiHu will collaborate to transition contracts to JiHu.

Q. **Who can I speak to at GitLab Inc. if I have questions not covered in this FAQ?**

Please see options for [contacting GitLab support](https://about.gitlab.com/support/#contact-support). Please note that GitLab Inc. is unable to speak on behalf of JiHu sales and support.

