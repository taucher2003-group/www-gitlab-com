# Friends and Family Day has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/company/family-and-friends-day](https://handbook.gitlab.com/handbook/company/family-and-friends-day)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/family-and-friends-day](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/family-and-friends-day)

If you need help or assistance with this, please reach out on Slack in
[`#handbook`](https://gitlab.slack.com/archives/C81PT2ALD), or to the
[current handbook DRI](https://handbook.gitlab.com/handbook/about/maintenance/).

