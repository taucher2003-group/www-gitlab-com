# <https://docs.gitlab.com/ee/user/project/code_owners.html>

# Each CODEOWNERS entry requires at least 2 or more individuals OR at least 1 group with access to the www-gitlab-com repository

# The order in which the paths are defined is significant: the last pattern that matches a given path will be used to find the code owners

[CODEOWNERS]
.gitlab/CODEOWNERS @laurenbarker @timzallmann @david @gitlab-com/office-of-the-ceo

# NB Do not move this entry from the top of this file

^[Default Description Templates]
.gitlab/issue_templates/Default.md @sytses @gitlab-com/office-of-the-ceo
.gitlab/merge_request_templates/Default.md @sytses @gitlab-com/office-of-the-ceo

^[about.gitlab.com Markdown Guide]
/sites/uncategorized/source/community/markdown-guide-middleman/ @gitlab-com/office-of-the-ceo

^[Active Tests]
/sites/uncategorized/source/pricing/ @akramer

^[Team Page & Pets]
/data/team_members/ @mpatel8 @ameeks @ashleyjones @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/images/team/ @mpatel8 @ameeks @ashleyjones @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/company/team-pets/ @mpatel8 @ameeks @ashleyjones @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov

[Tech Stack]
/data/tech_stack.yml @NabithaRao @marc_disabatino @ccurato

^[Compensation]
/data/currency_conversions.yml @wendybarnes @mwilkins
/data/entity_mapper.yml @wendybarnes @anechan
/data/country_payment_information.yml @wendybarnes @brobins
/data/variable_pay_frequency.yml @mwilkins

^[Engineering]
/data/projects.yml @joergheilig
/data/schemas/team_member.schema.json @joergheilig @sabrinafarmer @rymai

^[Learn at gitlab]
/source/images/learn/ @brobins

^[Marketing]
/data/redirects.yml @akramer
/data/webcasts.yml @aoetama
/sites/uncategorized/source/why/ @akramer
/sites/uncategorized/source/community/ @esalvadorp @akramer @nick_vh
/sites/uncategorized/source/community/contribute/ @esalvadorp @nick_vh @daniel-murphy
/sites/uncategorized/source/community/contribute/dco-cla/ @esalvadorp @dfrhodes @nick_vh
/sites/uncategorized/source/community/gitlab-first-look/ @ampesta
/sites/uncategorized/source/community/hackathon/ @esalvadorp @johncoghlan @nick_vh @leetickett-gitlab @daniel-murphy
/sites/uncategorized/source/community/issue-bash/ @sabrinafarmer @nick_vh
/sites/uncategorized/source/community/mvp/ @esalvadorp @nick_vh
/sites/uncategorized/source/community/ten-year/ @esalvadorp @johncoghlan
/sites/uncategorized/source/community/top-annual-contributors/ @esalvadorp @nick_vh
/sites/uncategorized/source/includes/contact-sales.html.haml @akramer
/sites/uncategorized/source/resources/ @aoetama
/sites/**/core-team/ @leetickett-gitlab @nick_vh

^[Marketing Community]
/data/speakers.yml @johncoghlan
/data/speakers_requirements.yml @johncoghlan

^[Product]
/data/categories.yml @david @gitlab-da @esalvadorp
/data/sections.yml @david @joergheilig
/data/stages.yml @david @joergheilig
/source/direction/ @david

^[Product - Dev Section]
/source/direction/dev/ @david @gl-product-leadership
/source/direction/dev/strategies/ @david @gl-product-leadership
/source/direction/foundations/ @mflouton @david @gl-product-leadership
/source/direction/foundations/personal_productivity/ @david @mflouton @gl-product-leadership
/source/direction/foundations/import_and_integrate/ @david @mflouton @m_frankiewicz @gl-product-leadership
/source/direction/plan/devops-reports/ @hsnir1 @david @gl-product-leadership
/source/direction/foundations/code-analytics/ @hsnir1 @david @gl-product-leadership
/source/direction/foundations/personal_productivity/notifications/ @gl-product-leadership @mflouton @david
/source/direction/foundations/personal_productivity/navigation/ @gl-product-leadership @mflouton @david
/source/direction/foundations/personal_productivity/settings/ @gl-product-leadership @mflouton @david
/source/direction/foundations/design_system/ @gl-product-leadership @mflouton @david
/source/direction/plan/ @gweaver @david @gl-product-leadership
/source/direction/plan/value_stream_management/ @gweaver @hsnir1 @david @gl-product-leadership
/source/direction/plan/portfolio_management/ @gweaver @david @amandarueda @gl-product-leadership
/source/direction/plan/design_management/ @gweaver @david @amandarueda @gl-product-leadership
/source/direction/plan/project_management/ @david @gweaver @gl-product-leadership
/source/direction/plan/project_management/team_planning/ @david @gweaver @gl-product-leadership
/source/direction/plan/knowledge/content_editor/ @gweaver @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/ @gweaver @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/wiki/ @gweaver @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/pages/ @gweaver @david @mmacfarlane @gl-product-leadership
/source/direction/create/ @derekferguson @steve-evangelista
/source/direction/create/source_code_management/ @mcbabin @derekferguson
/source/direction/create/source_code_management/source_editor/ @mcbabin @derekferguson
/source/direction/create/gitter/ @mcbabin @derekferguson
/source/direction/create/code_review_workflow/ @phikai @derekferguson
/source/direction/create/remote_development/ @michelle-chen @derekferguson
/source/direction/create/code_creation/code_suggestions/ @derekferguson @steve-evangelista
/source/direction/create/editor_extensions/ @dashaadu @derekferguson
/source/direction/create/gitlab_cli/ @phikai @derekferguson

^[Product Section - Ops]
/source/direction/ops/ @david @mflouton
/source/direction/delivery/ @nagyv-gitlab
/source/direction/package/ @trizzi
/source/direction/verify/ @mflouton
/source/direction/ci/ @mflouton
/source/direction/verify/code_testing/ @rutshah @mflouton
/source/direction/verify/job_artifacts/ @rutshah @mflouton
/source/direction/verify/performance_testing/ @rutshah @mflouton
/source/direction/verify/review_apps/ @mflouton
/source/direction/verify/hosted_runners/ @mflouton @gabrielengel_gl
/source/direction/verify/runner_core/ @DarrenEastman @mflouton
/source/direction/verify/fleet_visibility/ @DarrenEastman @mflouton

^[Product Section - Sec]
/source/direction/security/ @hbenson @gl-product-leadership
/source/direction/application_security_testing/ @sarahwaldner @hbenson
/source/direction/application_security_testing/static-analysis/ @connorgilbert @sarahwaldner
/source/direction/application_security_testing/secret-detection/ @abellucci @sarahwaldner
/source/direction/application_security_testing/dynamic-analysis/ @johncrowley @sarahwaldner
/source/direction/application_security_testing/vulnerability-research/ @sarahwaldner
/source/direction/application_security_testing/composition-analysis/ @johncrowley @sarahwaldner
/source/direction/software_supply_chain_security/ @sarahwaldner @hbenson @hsutor @jrandazzo
/source/direction/security_risk_management/security-insights/ @dagron1
/source/direction/security_risk_management/security_policies/ @g.hickman @dagron1
/source/direction/security_risk_management/security-platform-management/ @smeadzinger @dagron1
/source/direction/software_supply_chain_security/compliance/ @khornergit @sarahwaldner @hbenson
/source/direction/software_supply_chain_security/authentication/ @hsutor @sarahwaldner @hbenson
/source/direction/software_supply_chain_security/authorization/ @jrandazzo @sarahwaldner @hbenson
/source/direction/supply-chain/ @sarahwaldner @hbenson

^[Product - Analytics Section]
/source/direction/monitor/ @david @justinfarris @steve-evangelista
/source/direction/monitor/analytics-instrumentation/ @david @justinfarris @steve-evangelista @tjayaramaraju
/source/direction/monitor/platform-insights/ @lfarina8

^[Product - SaaS Platforms Section]
/source/direction/saas-platforms/ @fzimmer @david @gl-product-leadership
/source/direction/saas-platforms/dotcom/ @fzimmer @swiskow
/source/direction/saas-platforms/delivery/ @fzimmer @swiskow @mbruemmer
/source/direction/saas-platforms/production-engineering/ @fzimmer @swiskow
/source/direction/saas-platforms/dedicated/ @fzimmer @cbalane
/source/direction/saas-platforms/switchboard/ @fzimmer @lbortins
/source/direction/saas-platforms/tenant-scale/ @fzimmer @mjwood

^[Product - Core Platform Section]
/source/direction/core_platform/ @mflouton @david @gl-product-leadership
/source/direction/core_platform/tenant-scale/ @mflouton @lohrc
/source/direction/core_platform/tenant-scale/cell/ @mflouton @lohrc
/source/direction/core_platform/tenant-scale/organization/ @mflouton @lohrc
/source/direction/core_platform/tenant-scale/groups-&-projects/ @mflouton @lohrc
/source/direction/core_platform/tenant-scale/user-profile/ @mflouton @lohrc
/source/direction/geo/ @mflouton @sranasinghe
/source/direction/geo/geo_replication/ @mflouton @sranasinghe
/source/direction/geo/backup_restore/ @mflouton  @sranasinghe
/source/direction/geo/disaster_recovery/ @mflouton @sranasinghe
/source/direction/global-search/ @mflouton @bvenker
/source/direction/cloud-connector/ @mflouton @rogerwoo
/source/direction/distribution/ @mflouton @dorrino
/source/direction/core_platform/dotcom/ @mflouton
/source/direction/database/ @mflouton
/source/direction/gitaly/ @mflouton @mjwood

^[Product - Fulfillment Section]
/source/direction/fulfillment/ @courtmeddaugh
/source/direction/fulfillment/consumables-cost-management/ @courtmeddaugh
/source/direction/fulfillment/seat-cost-management/ @courtmeddaugh
/source/direction/fulfillment/subscription-management/ @tgolubeva
/source/direction/fulfillment/add-on-provisioning/ @ppalanikumar
/source/direction/fulfillment/plan-provisioning/ @ppalanikumar
/source/direction/fulfillment/fulfillment-admin-tooling/ @ppalanikumar
/source/direction/fulfillment/customers-dot-application/ @ppalanikumar
/source/direction/fulfillment/fulfillment-infrastructure/ @ppalanikumar
/sites/uncategorized/source/pricing/licensing-faq/cloud-licensing/ @ppalanikumar

^[Product Section - Data Science]
/source/direction/data-science/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/modelops/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ai_framework/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/duo_chat/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/ai_model_validation/ @tmccaslin @hbenson @gl-product-leadership

^[Product Section - Service Management]
/source/direction/service_management/ @hbenson

^[Product Section - Other]
/source/direction/product-analysis/ @cbraza @gl-product-leadership
/source/direction/mobile/mobile-devops/ @DarrenEastman @mflouton
/data/jtbd.yml @mvanremmerden @jmandell

^[Security Compliance]
/sites/uncategorized/source/security/ @joshlemos @jlongo_gitlab

^[Releases blog posts]
/sites/uncategorized/source/releases/posts/ @gitlab-org/delivery, @gl-product-leadership

^[Delivery team]
/data/releases.yml @gitlab-org/delivery

^[Speakers Bureau]
/sites/uncategorized/source/speakers/ @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/index.html.erb @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/data.json.erb @johncoghlan @dnsmichi
/data/speakers.yml @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/ @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/SpeakersBureau.vue @johncoghlan @dnsmichi

^[Environment Setup Script]
/scripts/setup-macos-dev-environment.sh @brobins

^[Ruby Version]
.ruby-version @laurenbarker @cwoolley-gitlab

# This is the Legal and Corporate Affairs section

[Legal and Corporate Affairs Documents]
/sites/uncategorized/source/terms/signature.html.md @robin @ktesh @m_taylor @gitlab-com/egroup
/sites/uncategorized/source/privacy/subprocessors/index.html.md @robin @ktesh @m_taylor @emccrann @amilidou @gitlab-com/egroup
