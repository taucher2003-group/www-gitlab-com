# Tech Stack - Add New System & System Onboarding
**Please do not merge before the Business Systems Analysts have reviewed and approved!**

**Questions? Ask in [#tech-owners_tech-stack](https://gitlab.enterprise.slack.com/archives/C027X43TLCE) Slack channel.**

## Business/Technical System Owner or Delegate to Complete

### General Tech Stack Entry Tasks
1. [ ] Rename this MR's title to `[System Name] - Tech Stack - Add New System & System Onboarding`
2. [ ] Requisition Link (if an externally-developed System):
3. [ ] Populate all data fields using the Web IDE. More instructions are [here](https://about.gitlab.com/handbook/business-technology/tech-stack-applications/#what-data-lives-in-the-tech-stack).
4. Is this New System replacing an existing System in the Tech Stack? 
    * [ ] Yes - Delete the existing System's entry from the Tech Stack in this MR using the Web IDE. Next, create a [Tech Stack Offboarding Issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new?issuable_template=offboarding_tech_stack).  Offboarding Issue Link:
    * [ ] No

### Access Tasks
1. [ ] Add the New System to **one** of two Offboarding templates below. More instructions are [here](https://about.gitlab.com/handbook/business-technology/tech-stack-applications/#updating-the-offboarding-templates).
    * [ ] Option 1 - Main [Team Member Offboarding](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) template
      * MR Link:
    * [ ] Option 2 - [Department-level Offboarding template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/offboarding_tasks) folder
      * MR Link:
2. **System Provisioners / Deprovisioners:**
   - [ ] @gl-people-connect-team is to update appropriate Offboarding Template (Add/Remove Team Member(s)).

### System Onboarding Checklist

_Each checklist item below should be addressed before this MR can be merged.  If unsure of whether a requirement is satisfied, ask the Technical Owner (if a different Team Member) or Vendor Contact / Sales Engineer. Reach out to Security Risk in the [#tech-owners_tech-stack](https://gitlab.enterprise.slack.com/archives/C027X43TLCE) Slack channel for help._

1. The New System is configured for [Okta Single Sign On](https://handbook.gitlab.com/handbook/business-technology/okta/#how-is-gitlab-using-okta).
    * [ ] Yes - [Paste Okta SSO Issue link](https://gitlab.com/gitlab-com/business-technology/change-management/-/issues/new?issuable_template=okta_new_app_request):
    * [ ] N/A 
      * Rationale (_Populate_):
2. [ ] Encryption of data in-transit and data at-rest are enabled for the New System.  *Note: Data encryption protocol is specified in the SOC 2, if available.*
3. [ ] GitLab's implementation of the New System has [audit logging](https://handbook.gitlab.com/handbook/security/security-and-technology-policies/audit-logging-policy/) enabled and documented.
4. If applicable, SOC 2 Complementary User Entity Controls (CUECs) have been reviewed and acknowledged by the Business/System Owner. _Note: Security Risk will address this item_.
    * [ ] Yes - Link to Comment in TPRM Assessment Report Issue indicating confirmation from the Business:
    * [ ] N/A 
      * Rationale (_Populate_):
5. [ ] I understand that the below-linked controls must be implemented if this System was to be in-scope for one of GitLab's external certifications such as [SOC](https://www.aicpa-cima.com/topic/audit-assurance/audit-and-assurance-greater-than-soc-2), [SOX](https://us.aicpa.org/advocacy/issues/section404bofsox#:~:text=The%20Sarbanes%2DOxley%20Act%20requires,assessment%20of%20its%20internal%20controls.), or [ISO](https://www.iso.org/standard/27001).
    * [Security Compliance General Controls Checklist](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance/compliance/-/issues/3418#top)
    * [Current list of systems "in-scope"](https://docs.google.com/spreadsheets/d/11_53R0QQLes0ZftjMlWwhyOUqhhuZFPcbVunkHNeF-U/edit#gid=0), for reference.
6. Does the system/application support accessibility features? 
    * [ ] Yes
    * [ ] No

    **If yes please document or provide a link for this systems accessibility features in the comments.**

## Privacy Team to Complete
If the New System contains [Personal Data](https://about.gitlab.com/handbook/security/data-classification-standard.html#data-classification-definitions), has a [Privacy Review been completed?](https://about.gitlab.com/handbook/legal/privacy/#privacy-review-process): 
* [ ] If System contains Orange (internal only) / RED Personal Data:
    * [ ] Yes - Link a completed Privacy Review Issue, Coupa approval, or Zip approval.
    * [ ] No - **Complete** **[Privacy Review Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=Vendor-Procurement-Privacy-Review)**
* [ ] If System contains Yellow Personal Data (GitLab Team Member Names/Emails):
    * [ ] Yes - a Data Processing Agreement (DPA) was executed between GitLab and the Vendor.
    * [ ] No - a DPA is not in place. Privacy Team will be in contact about completing a DPA, which is required for this Tech Stack Addition.
* [ ] If System contains only Green Data or contains no [Personal Data](https://about.gitlab.com/handbook/security/data-classification-standard.html#data-classification-definitions), a Privacy Review is not required.

## Security Risk Team to Complete
1. [ ] Check this box to **indicate approval** of the New System's [Critical System Tier](https://handbook.gitlab.com/handbook/security/security-assurance/security-risk/storm-program/critical-systems/#designating-critical-system-tiers).
2. Answer Question 4. in 'System Onboarding Checklist' section above.
3. Was a [Technical Security Validation](https://handbook.gitlab.com/handbook/security/security-assurance/security-risk/third-party-risk-management/#technical-security-validations) launched in response to the TPRM Assessment?
   * [ ] Yes - Link the TSV `here` and confirm all steps within the **Observation Management** section of the TSV have been completed, including acknowledgment of TSV findings by the Business Owner if findings were noted.
   * [ ] No - No further action needed.

## Business Technology Team to Complete
* [ ] To-do before merging -- (@marc_disabatino) is to ensure all sections/action items are completed.

/cc @gitlab-com/internal-audit @disla
/assign @marc_disabatino @emccrann
/labels ~BusinessTechnology ~BT-TechStack ~BT-TechStack-NewSystem ~"BT-TechStack::To do" ~"TPRM::Unassigned"
