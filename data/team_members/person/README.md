# Team member data schema

You can find instructions on how to update your own entry [in the handbook](https://handbook.gitlab.com/handbook/about/editing-handbook/edit-team-page/).

Every team member has their own file under: `{first_letter_of_slug}/{slug}.yml`.
So a team member with the slug `tanuki` can find their file under: `t/tanuki.yml`.
Here is a link to Sid's file: [`s/sid.yml`](s/sid.yml).

You can rename your file, but make sure that it is still in the correct folder.
Danger will let you know if you accidentally put a file in the wrong index directory.
So if `tanuki` wants to rename themselves to `japanese-raccoon-dog`, they need to move
their file from `t/tanuki.yml` to `j/japanese-raccoon-dog.yml`.

The `slug` is a human-readable unique id based on the person's name or a vacancy's title.
It is used for the `reports_to` property.

## Data structure

<!-- When modifying/adding new fields to this table, please list in alphabetical order with required options first. -->

| Property           | Required | Example             | Description |
|--------------------|----------|---------------------|-------------|
| `departments`      | `true`   | see below           | Array of strings containing the person's departments. First entry synced with Workday. Populates team page drop down. |
| `division`         | `true`   | `People Group`      | Person's current division in Workday, automatically updated if changed. Populates team page drop down. |
| `gitlab`           | `true`   | `dzaporozhets`      | A GitLab.com handle (without the @ symbol). Regularly synced with Workday. Can be omitted in certain circumstances, like for board members, advisors or core team members. |
| `job_title`        | `true`   | `Backend Engineer`  | Person's current job title in Workday, automatically updated if changed. |
| `name`             | `true`   | `Eric 'EJ' Johnson` | Person's name as the person wants it to appear on the team page. It's fine to add different scripts than Latin, or abbreviate parts of your name if you do not want them to appear publicly. |
| `public`           | `true`   | true                | Person's team page export preferences in Workday, edit this field in Workday to update. |
| `reports_to`       | `true`   | `sid`               | A person slug, will be validated to exist. Can be omitted in certain circumstances, like for board members, advisors or core team members. This slug is the name of the manager's `yml` file. |
| `role`             | `true`   | see below           | HTML link describing the role. Link to the full title including specialty. |
| `borrow`           | `false`  | see below           | An object with the `to` and `end_date` keys. Only applicable when the team member is borrowed to a team. `to` is the borrowing manager' `slug` (it is validated to exist). `end_date` is the date until when the team member is borrowed. Should follow the `YYYY-MM-DD` format. |
| `country`          | `false`  | `Ukraine`           | The text of the country of the person. Can be left empty. If the country is empty, the person will not appear on the map |
| `domain_expertise` | `false`  | see below           | Array of expertises from [`domain_expertise.yml`][domain_expertise]. |
| `expertise`        | `false`  | `<b>HTML</b>`       | HTML of the person's expertises. |
| `linkedin`         | `false`  | `dzaporozhets`      | The url fragment for [your LinkedIn profile][linkedin-profile-url]. Use only the part that comes after `/in/` |
| `locality`         | `false`  | `Kharkiv`           | The text location within a country of the person. |
| `mastodon`         | `false`  | `mstdn.gl/@dzaporozhets` | Mastodon profile link without the `https://`. |
| `mentor`           | `false`  | true                | Identify as a mentor. Will link to https://handbook.gitlab.com/handbook/engineering/careers/mentoring/ |
| `picture`          | `false`  | `dmitriy.png`       | The filename of a picture in [`/sites/uncategorized/source/images/team`][images]. Ensure that this is a square and <= 400x400 px. |
| `projects`         | `false`  | see below           | Object of values from [`projects.yml`][projects], together with the role in the projects. |
| `pronouns`         | `false`  | `she/her`           | Preferred pronouns.  Will link to <http://pronoun.is/>, for example, <https://pronoun.is/she/her>. |
| `pronunciation`    | `false`  | `sean kah-rul`      | Free text field to help with pronunciation of your name. Can be pseudo-pronunciation, contain IPA or links.  |
| `remote_story`     | `false`  | `A long story`      | Person's remote story, will be published on: <https://about.gitlab.com/company/culture/all-remote/stories/>. |
| `specialty`        | `false`  | see below           | A string or array of strings containing the person's specialty. Synced with Workday. |
| `story`            | `false`  | `Some string`       | Text of the person's biography. |
| `team_tags`        | `false`  | see below           | Array of Strings containing the person's Engineering stage/group teams, use the same naming as shown in `*_team_tag` (such as `fe_team_tag`) in [`stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) or [`sections.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/sections.yml). |
| `twitter`          | `false`  | `dzaporozhets`      | A Twitter handle (without the `@` symbol). |
| `work_priorities`  | `false`  | see below           | An array of current work priorities. |


- Example for `role`:

  - `<a href="https://handbook.gitlab.com/job-families/engineering/backend-engineer/">Staff Backend Engineer</a>`
  - `<a href="https://handbook.gitlab.com/job-families/engineering/development/management/#senior-engineering-manager">Senior Engineering Manager, Fulfillment</a>`

- Example for `borrow`:

    ```yaml
    borrow:
      to: ramya-authappan
      end_date: 2023-09-15
    ```

- Example for `departments`:

    ```yaml
    departments:
      - People Success
    ```

    ```yaml
    departments:
      - Development Department
      - Fulfillment Section
      - Fulfillment Sub-department
      - Fulfillment:Purchase Team
      - Backend
    ```

- Example for `team_tags`

  ```yaml
  team_tags:
    - Composition Analysis Group CS Stable Counterpart
    - Dynamic Analysis Group CS Stable Counterpart
    - Secret Detection Group CS Stable Counterpart
    - Static Analysis Group CS Stable Counterpart
  ```

- Example for `work_priorities`:

    ```yaml
    work_priorities:
      - Product Analytics
      - ModelOps
    ```

    To see accepted **case-sensitive** values, see `data/schemas/team_member.schema.json`.

- Example for `specialty`:

    ```yaml
    specialty:
      - 'Fulfillment'
      - 'Fulfillment: Purchase'
      - 'Fulfillment: License'
      - 'Fulfillment: Utilization'
    ```

- Example for `projects`:

    ```yaml
    # Values may either be a string or an array of strings
    # if specifying more than one role for a project.
    projects:
      gitlab:
        - maintainer backend
        - maintainer database
      gitlab-gollum-lib: reviewer backend
    ```

- Example for `domain_expertise`:

    ```yaml
    domain_expertise:
      - rails
      - graphql
      - design_management
      - create
    ```

- Example for `expertise`:

    ```yaml
    expertise: |
      <li>Item 1</li>
      <li>Item 2</li>

- Note for `public`:

You can find a "How to" guide for setting the export preferences in Workday by searching for the ["How to: Set Team Page Export Preferences" Google doc](https://drive.google.com/drive/search?q=how+to+Set+Team+Page+Export+Preferences) (internal).

[projects]: ../../projects.yml
[domain_expertise]: ../../domain_expertise.yml
[images]: ../../../sites/uncategorized/source/images/team
[linkedin-profile-url]: https://www.linkedin.com/help/linkedin/answer/49315/find-your-linkedin-public-profile-url
