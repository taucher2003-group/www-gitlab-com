---
features:
  primary:
  - name: "New user contribution and membership mapping available in direct transfer"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/project/import/#user-contribution-and-membership-mapping'
    image_url: '/images/17_7/user_contributions_mapping.png'
    reporter: m_frankiewicz
    stage: foundations
    categories: [Importers]
    epic_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/478054'
    description: |
      The new method of user contribution and membership mapping is now available when you migrate between GitLab instances by [direct transfer](https://docs.gitlab.com/ee/user/group/import/index.html). This feature offers flexibility and control for both users managing the import process and users receiving contribution reassignments. With the new method, you can:

      - Reassign memberships and contributions to existing users on the destination instance after the import has completed. Any memberships and contributions you import are first mapped to placeholder users. All contributions appear associated with placeholders until you reassign them on the destination instance.
      - Map memberships and contributions for users with different email addresses on source and destination instances.

      When you reassign a contribution to a user on the destination instance, the user can accept or reject the reassignment.

      For more information, see [streamline migrations with user contribution and membership mapping](https://about.gitlab.com/blog/2024/11/25/streamline-migrations-with-user-contribution-and-membership-mapping/). To leave feedback, add a comment to [issue 502565](https://gitlab.com/gitlab-org/gitlab/-/issues/502565).
