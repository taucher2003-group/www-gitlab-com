---
features:
  top:
  - name: "GitLab Duo Chat available in Beta"
    available_in: [gold]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html'
    video: 'https://www.youtube-nocookie.com/embed/l6vsd1HMaYA'
    reporter: tlinz
    stage: ai-powered
    categories:
    - Duo Chat
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/10550'
    description: |
     Everyone involved in the software development process can spend a significant amount of time familiarizing themselves with code, epics, issues, and lengthy discussion threads. You can often find yourself slowed down by routine tasks like writing summaries, documentation, tests, or even code. Having an expert at your side that can answer DevSecOps questions without judgment and address follow-ups could help you accelerate the software development process.

     GitLab Duo Chat aims to actively address these pain points and accelerate your workflows. Its capabilities include:

      - Explain or summarize issues, epics, and code.
      - Answer specific questions about these artifacts like "Collect all the arguments raised in comments regarding the solution proposed in this issue."
      - Generate code or content based the information in these artifacts. For instance, "Can you write documentation for this code?"
      - Or simply get you started from scratch like "Create a .gitlab-ci.yml configuration file for testing and building a Ruby on Rails application in a GitLab CI/CD pipeline."
      - Answer all your DevSecOps related question, whether you are beginner or an expert. For example, "How can I set up Dynamic Application Security Testing for a REST API?"
      - Answer follow-up questions so you can iteratively work through all the above scenarios.

      GitLab Duo Chat is available on GitLab.com as a [Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta) feature. It is also integrated into our Web IDE and GitLab Workflow extension for VS Code as [Experimental](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) features.

      You can also help us mature these features by providing feedback about your experiences with Duo Chat, either within the product or via our [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/430124).
