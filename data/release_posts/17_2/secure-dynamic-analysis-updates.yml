---
features:
  secondary:
  - name: "DAST analyzer updates"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/dast/browser/checks/'
    reporter: smeadzinger
    stage: application_security_testing
    categories:
    - 'DAST'
    issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13411'
    description: |
      During the 17.2 release milestone, we published the following updates.  
      
      1. We added three new checks:
      
      - Check 506.1 is a passive check that identifies request URLs that are likely compromised by the Polyfill.io CDN takeover. 
      - Check 384.1 is a passive check that identifies session fixation weaknesses, which could allow a valid session identifier to be reused by malicious actors. 
      - Check 16.11 is an active check that identifies when the TRACE HTTP debugging method is enabled on a production server, which could inadvertently expose sensitive information.

      2. We addressed the following bugs to reduce false positives:

      - DAST checks 614.1 (Sensitive cookie without Secure attribute) and 1004.1 (Sensitive cookie without HttpOnly attribute) no longer create findings when a site has cleared a cookie by setting an expiry date in the past.
      - DAST check 1336.1 (Server-Side Template Injection) no longer relies on a 500 HTTP response status code to determine attack success.

      3. We added the following enhancements:
      
      - All response headers are now presented as evidence in a DAST vulnerability finding. This additional context reduces time spent on triaging findings.
      - Sitemap.xml files are now crawled for additional URLs, leading to better coverage of target websites.
