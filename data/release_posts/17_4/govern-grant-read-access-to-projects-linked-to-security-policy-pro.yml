---
features:
  secondary:
  - name: "Grant read access to pipeline execution YAML files in projects linked to security policies"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/'
    image_url: '/images/17_4/grant-access-to-spp.png'
    reporter: g.hickman
    stage: software_supply_chain_security
    categories:
    - Security Policy Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/469439'
    description: |
      In GitLab 17.4, we added a setting to security policies you can use to grant read access to `pipeline-execution.yml` files for all linked projects. This setting gives you more flexibility to enable users, bots, or tokens that enforce pipeline execution globally across projects. For example, you can ensure a group or project access tokens can read security policy configurations in order to trigger pipelines during pipeline execution. You still can't view the security policy project repository or YAML directly. The configuration is used only during pipeline creation.

      To configure the setting, go to the security policy project you want to share. Select **Settings > General > Visibility, project features, permissions**, scroll to **Pipeline execution policies**, and enable the **Grant access to this repository for projects linked to it as the security policy project source for security policies** toggle.
