---
features:
  secondary:
  - name: "Enforce custom stages in pipeline execution policies"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html#inject_policy'
    image_url: '/images/17_9/custom-stages-yaml.png'
    reporter: g.hickman
    stage: security_risk_management
    categories:
    - Security Policy Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/475152'
    description: |
      We're excited to introduce a new capability for pipeline execution policies that allows you to enforce **custom stages** into your CI/CD pipelines in `Inject` mode. This feature provides greater flexibility and control over your pipeline structure while maintaining security and compliance requirements, supplying you with:

      * **Enhanced pipeline customization**: Define and inject custom stages at specific points in your pipeline, allowing for more granular control over job execution order.
      * **Improved security and compliance**: Ensure that security scans and compliance checks run at the most appropriate times in your pipeline, such as after build but before deployment.
      * **Flexible policy management**: Maintain centralized policy control while allowing development teams to customize their pipelines within defined guardrails.
      * **Seamless integration**: Custom stages work alongside existing project stages and other policy types, providing a non-disruptive way to enhance your CI/CD workflows.

      **How does it work?**

      The new and improved `inject_policy` strategy for pipeline execution policies allows you to define custom stages in your policy configuration. These stages are then intelligently merged with your project's existing stages using a Directed Acyclic Graph (DAG) algorithm, ensuring proper ordering and preventing conflicts.

      For example, you can now easily inject a custom security scanning stage between your build and deploy stages.

      The `inject_policy` stage replaces `inject_ci` which will be deprecated, allowing you to opt into the `inject_policy` mode to gain the benefits. The `inject_policy` mode will become the default when configuring policies with `Inject` in the policy editor.
