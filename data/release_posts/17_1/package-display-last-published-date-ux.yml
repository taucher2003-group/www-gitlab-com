---
features:
  secondary:
  - name: "Display the last published date for container images"
    available_in: [free, silver, gold]
    documentation_link: 'https://docs.gitlab.com/ee/user/packages/container_registry/#view-the-container-registry'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/290949'
    description: |
      Previously, the published timestamp was often incorrect in the container registry user interface. This meant that you couldn't rely on this important data to find and validate your container images.

      In GitLab 17.1, we've updated the UI to include accurate `last_published_at` timestamps. You can find this information by navigating to **Deploy > Container Registry** and selecting a tag to view more details. The last published date is available at the top of the page.

      This improvement is generally available only on GitLab.com. Self-managed support is in beta and available only on instances that have enabled the beta [next-generation container registry](https://docs.gitlab.com/ee/administration/packages/container_registry_metadata_database.html).
