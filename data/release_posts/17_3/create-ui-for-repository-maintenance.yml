features:
  secondary:
  - name: "More easily remove content from repositories"
    available_in: [core, premium, ultimate]  # Include all supported tiers
    documentation_link: 'https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html#remove-blobs'
    gitlab_com: true
    image_url: '/images/17_3/create-ui-for-repository-maintenance.png'
    reporter: mcbabin
    stage: create
    categories:
    - 'Source Code Management'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
    - 'https://gitlab.com/gitlab-org/gitlab/-/issues/450701'
    description: |
      Currently, the process for removing content from a repository is complicated, and you might have to force push the project to GitLab.
      This is prone to errors and can cause you to temporarily turn off protections to enable the push.
      It can be even harder to delete files that use too much space within the repository.

      You can now use the new repository maintenance option in project settings to remove blobs based on a list of object IDs.
      With this new method, you can selectively remove content without the need to force push a project back to GitLab.

      In the event that secrets or other content has been pushed that needs to be redacted from a project, we're also introducing a new option to redact text.
      Provide a string that GitLab will replace with `***REMOVED***` in files across the project.
      After the text has been redacted, run housekeeping to remove old versions of the string.

      This new UI streamlines the way you can manage your repositories when content needs to be removed.
