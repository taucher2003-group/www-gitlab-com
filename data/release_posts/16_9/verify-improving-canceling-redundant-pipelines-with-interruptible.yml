---
features:
  primary:
  - name: "Expanded options for auto-canceling pipelines"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/index.html#workflowauto_cancelon_new_commit'
    image_url: '/images/16_9/interruptible.png'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Composition
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/412473'
    description: |
      Currently, to use the [auto-cancel redundant pipeline feature](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines), you must set jobs that can be cancelled as [`interruptible: true`](https://docs.gitlab.com/ee/ci/yaml/index.html#interruptible) to determine whether or not a pipeline can be cancelled. But this only applies to jobs that are actively running when GitLab tries to cancel the pipeline. Any jobs that have not yet started (are in "pending" status) are also considered safe to cancel, regardless of their `interruptible` configuration.

      This lack of flexibility hinders users who want more control over which exact jobs can be cancelled by the auto-cancel pipeline feature. To address this limitation, we are pleased to announce the introduction of the `auto_cancel:on_new_commit` keywords with more granular control over job cancellation. If the legacy behavior did not work for you, you now have the option to configure the pipeline to only cancel jobs that are explicitly set with `interruptible: true`, even if they haven't started yet. You can also set jobs to never be automatically cancelled.
