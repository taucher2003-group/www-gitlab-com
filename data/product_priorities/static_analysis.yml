research_priorities:
- name: "Release rule updates based on benchmark analysis"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14685"
  status: "Expected to complete in 17.9/17.10. In progress."
  one_month: "Ship Python updates. Refresh Go ground-truth and ship updates. Ship already-drafted Java updates only (with further Java updates later)."

- name: "Create Advanced SAST ruleset for PHP"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14273"
  status: "Expected in FY26Q1. In progress."
  three_month: "Migrate existing rules; develop new rules; analyze performance."

- name: "Address false-negative results in C# Advanced SAST coverage"
  url: "https://gitlab.com/gitlab-org/gitlab/-/issues/509461"
  status: "Expected by FY26Q2."
  three_month: "Analyze existing cases; diagnose gaps; analyze and improve source/sink coverage; analyze and improve rule coverage"

- name: "Update Java rules based on benchmark/example analysis"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14685"
  status: "To be scheduled. Will involve refreshing our ground-truth analysis and implementing rule changes."

- name: "Create Advanced SAST ruleset for C++"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14271"

- name: "Expand detection of dangerous query construction without traceable user input"
  url: "https://gitlab.com/gitlab-org/gitlab/-/issues/500376"

- name: "Implement the next level of documentation for rule/CWE coverage"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15343"
  status: "Assessing implementation options."
  one_month: "Interview internal users and develop technical plan"
  three_month: "Ship documentation"

###################################
docs_priorities:
- name: "Provide guidance on how to evaluate GitLab SAST"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15295"
  status: "Initial [guide](https://docs.gitlab.com/ee/user/application_security/sast/evaluation_guide.html) shipped"
  one_month: "Implement further edits to the evaluation guide"
  three_month: "Publish benchmark/example project guide, based on [analysis project listed below](https://gitlab.com/groups/gitlab-org/-/epics/14685)"

- name: "Restructure and update Advanced SAST docs now that the feature is GA"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15401"
  status: "In progress. (Primarily documentation.)"
  one_month: "Complete most issues in this epic"
  three_month: "Complete entire epic"

###################################
priorities:
- name: "Advanced SAST support for PHP"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14273"
  status: "Expected in FY26Q1. In progress."
  one_month: "Finalize engine support, implement rules"
  three_month: "Complete rules; test; identify any additional changes required"

- name: "Duo Vulnerability Resolution: Support new single-file vulnerability types"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16060"
  status: "Expected in FY26Q1. In progress."
  one_month: "Complete evaluation. Begin enabling vuln types that pass the quality standard."

- name: "New metrics for SAST adoption"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16661"
  status: "Expected in 17.10. Define technical plan and implement in 17.10."
  one_month: "Implement high-priority missing metrics"

- name: "Proactive detection accuracy updates for Python, Go, Java"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14685"
  status: "Expected FY26Q1. (Primarily Vulnerabilty Research.)"
  one_month: "Ship findings based on analysis of benchmark/example applications"

- name: "Multi-core Advanced SAST scanning"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16850"
  status: "Expected in FY26Q1. Available as an opt-in."
  one_month: "Enable by default"

- name: "Improve Advanced SAST performance and stability" # Consider removing, as this duplicates maintenance tasks
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16560"
  status: "Beginning implementation in 17.10."
  one_month: "Begin implementation"
  three_month: "Differential-scanning, multi-threaded engine, incremental scanning"

- name: "Enable Advanced SAST by default"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15145"
  status: "Expected in 18.0 (FY26Q2)."
  one_month: "Make necessary preparations"
  three_month: "Complete transition"

- name: "Implement Advanced SAST for C/C++" # Note: priority reflects _time to work_, not _time to deliver_
  url: "https://gitlab.com/groups/gitlab-org/-/epics/14271"
  status: "Expected by FY26Q4. Beginning technical planning in 17.10."
  one_month: "Create technical plan"

- name: "Use Advanced SAST engine and rules for real-time IDE SAST scanning"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15384"
  status: "Expected in FY26Q2."
  one_month: "Use Advanced SAST engine; identify action items from user feedback"
  three_month: "Work toward self-managed support; address other user feedback"

- name: "Incremental scanning for Advanced SAST (skip unchanged code)"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15545"
  status: "Expected FY26Q2. Reassessing technical plan."

- name: "Reduce false negatives in C# Advanced SAST"
  url: "https://gitlab.com/gitlab-org/gitlab/-/issues/509461"
  status: "Expected FY26Q2. (Primarily Vulnerabilty Research.)"

- name: "Real-time IDE SAST scanning: Beta release"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15386"
  status: "Expected FY26Q3"

- name: "Customizable detection logic for Advanced SAST"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16604"
  status: "Expected FY26Q3"

- name: "Real-time IDE SAST scanning: GA release"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/16851"
  status: "Expected FY26Q4"

- name: "Duo Vulnerability Resolution: Support resolving cross-file injection vulnerabilities"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/15716"
  status: "Expected FY26Q4. Will require coordination with Security Risk Management."
