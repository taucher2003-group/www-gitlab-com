---
layout: markdown_page
title: "Data Unification Direction"
description: "GitLab Platform Insights Data Unification Direction Page"
---

- TOC
{:toc}

We would appreciate your feedback on this direction page. Please create an issue to collaborate with us or propose a MR to this page!

## Overview

Data is everywhere. From observability telemetry (metrics, traces, and logs), to application events, to DevOps metrics, to product analytics data, organizations need a way to process, store, and manage this explosion of data. Furthermore, organizations need the data to be available, without expensive or time-consuming ELT processes, for analysis. Historically, organizations either hired observability vendors or built and deployed their own monitoring/observability solutions. Both options are expensive and create data silos that are separate from the users' DevOps process. We view having the capability to process, store, manage, and access data as a critical part of the DevOps platform.

## Mission & Vision

### Mission

Process, store, manage, and access the data from your entire software development life-cycle in the complete DevOps platform.

### Vision

TBD