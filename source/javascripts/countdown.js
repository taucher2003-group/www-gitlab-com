/* eslint-disable no-unused-vars */

function getTime(distance) {
  const timeUnits = {
    days: Math.floor(distance / (1000 * 60 * 60 * 24)),
    hours: Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
    minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
    seconds: Math.floor((distance % (1000 * 60)) / 1000)
  };
  return timeUnits;
}

function formatTimeString(timeUnits) {
  return `${timeUnits.days} days ${timeUnits.hours} hours ${timeUnits.minutes} minutes ${timeUnits.seconds} seconds`;
}

function setupCountdown(countDownDate, countdownElementId) {
  const element = document.getElementById(countdownElementId);
  
  var x = setInterval(function() {
    var now = new Date().getTime();
    var distanceToEnd = countDownDate.end - now;

    // If the count down is over, write some text
    if (distanceToEnd < 0) {
      clearInterval(x);
      element.innerHTML = 'The next hackathon is around the corner';
    } else {
      var distanceToStart = countDownDate.start - now;
      if (distanceToStart < 0) {
        const endTime = getTime(distanceToEnd);
        element.innerHTML = 
          `<div>Hackathon in progress!</div>
          <div>Ends in: ${formatTimeString(endTime)}</div>`;
      } else {
        const startTime = getTime(distanceToStart);
        element.innerHTML = formatTimeString(startTime);
      }
    }
  }, 1000);
}

window.setupCountdown = setupCountdown;
