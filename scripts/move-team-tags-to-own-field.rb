#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'yaml'
require 'fileutils'

class MoveTeamPageToOwnField
  def initialize
    team_tags
    @new_custom_tags = []
  end

  def move
    team_entries.each do |path|
      content = load_yaml_file(path)
      next unless content['departments'].size > 1 # skip only workday departments in list
      next unless content['departments'].any? { |d| team_tags.include?(d) }

      workday_department = content['departments'].first
      tags = content['departments'].select { |d| team_tags.include?(d) }.uniq.sort
      extra_dept_groups = content['departments'].select { |d| !tags.include?(d) && !team_tags.include?(d) && d != workday_department }.uniq
      new_data = { 'departments' => [workday_department, *extra_dept_groups] }

      # only include the new fields if applicable
      new_data['team_tags'] = tags unless tags.empty?
      # new_data['custom_tags'] = extra_dept_groups unless extra_dept_groups.empty?
      updated_content = content.merge(**new_data)

      # Add to list of all custom tags
      @new_custom_tags << updated_content['custom_tags']

      # Save the updated yaml file
      save_yml(path, updated_content)
    end

    # save_new_custom_tags
  end

  # Save all custom tags to `data/custom_tags.yml`
  def save_new_custom_tags
    save_yml('data/custom_tags.yml', @new_custom_tags.flatten.compact.uniq.sort)
  end

  # List all team tags from `data/stages.yml` and `data/sections.yml`
  def team_tags
    @team_tags ||= begin
      # Get team tags from stages.yml
      stages = load_yaml_file('data/stages.yml')['stages']
      stage_team_tags = stages.each_with_object([]) do |(id, stage), tags|
        stage['groups'].each_value do |group|
          group_tags = [
            group['be_team_tag'],
            group['fe_team_tag'],
            group['cs_team_tag'],
            group['set_team_tag'],
            group['sre_team_tag']
          ]
          tags.concat(group_tags)
        end
      end.compact.uniq

      # Get team tags from sections.yml
      sections = load_yaml_file('data/sections.yml')
      section_team_tags = sections.values.each_with_object([]) do |section, tags|
        section_tags = [
          section['ar_team_tag'],
          section['development_team_tag'],
          section['legal_team_tag'],
          section['pm_team_tag'],
          section['sre_team_tag'],
          section['support_team_tag']
        ]
        tags.concat(section_tags)
      end.compact.uniq

      # Combine and sort all team tags
      (stage_team_tags + section_team_tags).uniq.sort
    end
  end

  private

  def save_yml(path, data)
    File.binwrite(path, YAML.dump(data))
  end

  def load_yaml_file(path)
    YAML.unsafe_load(File.read(path))
  end

  def team_entries
    Dir['data/team_members/person/**/*.yml']
  end
end

MoveTeamPageToOwnField.new.move
